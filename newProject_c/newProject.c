//------------------------------------------------------------------------------
// Copyright (c) 2016, Michel RIZZO (the 'author' in the following)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: newProject
// Utility to create tree structure and source files from user's templates.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "newProject_cmdline.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define NEWPROJECT 				"newProject.jar"
#define BINDIR					"/newProject_bin/"
#define DISPLAY 				"DISPLAY"
#define JAVA 					"java"
#define ProcessPseudoFileSystem	"/proc/self/exe"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#define error(fmt, ...) do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define EXIST(x)		(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)		(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern void cmdline_parser_newProject_print_version();
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static const char *jarFiles[] = {
	NEWPROJECT,
	"commons-cli-1.3.1.jar",
	"newProject.jar",
	"swt-462.jar",
	""
};
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void getJARversion(char *jarpath, char *jar, char *build) {
	char command[4096];
	FILE *fdcmd = NULL;

	sprintf(command, "%s -cp %s -jar %s%s --version", JAVA, jarpath, jarpath, jar);
	fdcmd = popen(command, "r");
	if (fgets(build, BUFSIZ, fdcmd)) { ; }
	pclose(fdcmd);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Return code (EXIT_SUCCESS - Normal program stop / EXIT_FAILURE - Program stop on error)
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	struct stat locstat, *ptlocstat;        // stat variables for EXIST*
	char jarpath[PATH_MAX];
	char jarfile[PATH_MAX];
	char command[4096];
	char build[BUFSIZ];
	char *pt;
	unsigned int i;
	int j, retval = 0;
	size_t k;

	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_newProject(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	//----- Get real path of this executable to compute the path JARS files
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	if (readlink(ProcessPseudoFileSystem, jarpath, sizeof(jarpath)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		goto ERR;
	}
	dirname(jarpath);
	strncat(jarpath, BINDIR, sizeof(jarpath));
	if (! EXISTDIR(jarpath)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", jarpath);
		goto ERR;
	}
	//---- Check consistency of the installation
	i = 0;
	while (strlen(jarFiles[i]) != 0) {
		snprintf(jarfile, sizeof(jarfile), "%s%s",jarpath, jarFiles[i]);
		if (! EXIST(jarfile)) {
			error("WRONG INSTALLATION - Jar file '%s' does not exist.", jarfile);
			goto ERR;
		}
		++i;
	}
	//---- Option "--jars" (-j) -------------------------------------------------
	if (args_info.jars_given) {
		cmdline_parser_newProject_print_version();
		getJARversion(jarpath, (char *)NEWPROJECT, build);
		fprintf(stdout, "newProject - %s", build);
		goto END;
	}
	//---- Check consistency of the environment
	if (NULL == getenv(DISPLAY)) {
		error("%s", "WRONG ENVIRONMENT - DISPLAY not initialized.");
		goto ERR;
	}
	//---- Run Java
	sprintf(command, "%s -cp %s -jar %s%s", JAVA, jarpath, jarpath, NEWPROJECT);
	for (j = 1; j < argc; j++) {
		strcat(command, " ");
		pt = argv[j];
		k = strlen(command);
		while (*pt != '\0') {
			if (*pt == ' ') command[k++] = '\\';
			command[k] = *pt;
			command[++k] = '\0';
			++pt;
		}
	}
	retval = system(command);
	//---- End -------------------------------------------------------------------
END:
	cmdline_parser_newProject_free(&args_info);
	return(retval);
ERR:
	cmdline_parser_newProject_free(&args_info);
	return EXIT_FAILURE;
}
