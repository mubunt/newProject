#-------------------------------------------------------------------------------
# Copyright (c) 2016-2017, Michel RIZZO (the 'author' in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: newProject
# Utility to create tree structure and source files from user's templates.
#-------------------------------------------------------------------------------
BUILD_REL	= 2
BUILD_LEV	= 4
BUILD_MIN	= 5
#-------------------------------------------------------------------------------
MUTE		= @
#-------------------------------------------------------------------------------
help:
	@echo
	@echo ------------------
	@echo Available targets:
	@echo ------------------
	@echo
	@echo $(MAKE) all ........... Build project outputs
	@echo $(MAKE) clean ......... Clean project outputs
	@echo $(MAKE) install ....... Install project outputs INSTALLDIR=/...
	@echo $(MAKE) cleaninstall .. Remove installed project outputs INSTALLDIR=/...
	@echo
all clean:
	$(MUTE)cd  newProject_c; $(MAKE) --no-print-directory $@ BUILD_REL=$(BUILD_REL) BUILD_LEV=$(BUILD_LEV) BUILD_MIN=$(BUILD_MIN); cd ..
	$(MUTE)cd  newProject_java; $(MAKE) --no-print-directory $@; cd ..
install cleaninstall:
	$(MUTE)cd newProject_bin; $(MAKE) --no-print-directory $@; cd ..
	$(MUTE)cd newProject_data; $(MAKE) --no-print-directory $@; cd ..
#-------------------------------------------------------------------------------
.PHONY: all clean install cleaninstall help
#-------------------------------------------------------------------------------
