//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
class newProjectParameterFile {
    private static String Filelocation;
    private static String Filesourcename;
    private static String basesourcename;
    private static String dirlist;
    private static String targetlib;
    private static String sepecho;
    private static String sourcelist;
    private static String libraries;

    void init() {
        libraries = sourcelist = sepecho = targetlib = dirlist = basesourcename = Filesourcename = Filelocation = "[TO BE FILLED]";
    }

    String getFilelocation() {
        return Filelocation;
    }
    void setFilelocation(String filelocation) {
        this.Filelocation = filelocation;
    }
    String getFilesourcename() {
        return Filesourcename;
    }
    void setFilesourcename(String filesourcename) {
        this.Filesourcename = filesourcename;
    }

    String getBasesourcename() {
        return basesourcename;
    }
    void setBasesourcename(String basesourcename) {
        this.basesourcename = basesourcename;
    }

    String getDirlist() {
        return dirlist;
    }
    void setDirlist(String dirlist) {
        this.dirlist = dirlist;
    }

    String getTargetlib() {
        return targetlib;
    }
    void setTargetlib(String targetlib) {
        this.targetlib = targetlib;
    }

    String getSepecho() {
        return sepecho;
    }
    void setSepecho(String sepecho) {
        this.sepecho = sepecho;
    }

    String getSourceList() {
        return sourcelist;
    }
    void setSourceList(String sourcelist) {
        this.sourcelist = sourcelist;
    }

    String getLibraries() {
        return libraries;
    }
    void setLibraries(String libraries) {
        this.libraries = libraries;
    }
}
//==============================================================================
