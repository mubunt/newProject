//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
// 
// This program is free software; you can redistribute it and/or modify it under the terms
// of the GNU General Public License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

class newProjectGenProject {
	private final static String slash = System.getProperty("file.separator");
	//--------------------------------------------------------------------------
	static void genProject() {
		newProjectParameterFile block = new newProjectParameterFile();
		String projectroot = newProject.ProjectLocation + slash + newProject.ProjectName;
		newProject.printHEADERConsole("Project version management.");
		String gitDir = newProject.Git + slash + newProject.ProjectName + ".git";
		newProject.printITEMConsole("|    Creating GIT " + gitDir + " project");
		if (!makedir(gitDir)) {
			newProject.ErrorBox("Cannot create directory ", gitDir);
			newProject.printERRORConsole();
			return;
		}
		newProject.printDONEConsole();
		newProject.printITEMConsole("|    Initializing GIT " + gitDir + " project");
		String output = executeCommand("git init --bare " + gitDir);
		newProject.printDONEConsole();
		if (output.length() != 0)
			newProject.printlnConsole(output);
		newProject.printHEADERConsole("Project creation.");
		newProject.printITEMConsole("|    Cloning " + newProject.ProjectName + " directory");
		output = executeCommand("git clone " + gitDir + " " + projectroot);
		newProject.printDONEConsole();
		if (output.length() != 0)
			newProject.printlnConsole(output);
		newProject.printHEADERConsole("Top-level files.");
		block.init();
		block.setFilelocation(projectroot);
		if (newProject.README) {
			newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/README file");
			block.setFilesourcename("README");
			if (!newProjectGenFiles.genTemplate(9, block))
				return;
			newProject.printDONEConsole();
		}
		if (newProject.TODO) {
			newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/TODO file");
			block.setFilesourcename("TODO");
			if (!newProjectGenFiles.genTemplate(10, block))
				return;
			newProject.printDONEConsole();
		}
		if (newProject.ChangeLog) {
			newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/ChangeLog file");
			block.setFilesourcename("ChangeLog");
			if (!newProjectGenFiles.genTemplate(11, block))
				return;
			newProject.printDONEConsole();
		}
		if (newProject.RELEASENOTES) {
			newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/ReleaseNotes file");
			block.setFilesourcename("ReleaseNotes");
			if (!newProjectGenFiles.genTemplate(12, block))
				return;
			newProject.printDONEConsole();
		}
		if (newProject.gitignore) {
			newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/.gitignore file");
			block.setFilesourcename(".gitignore");
			if (!newProjectGenFiles.genTemplate(23, block))
				return;
			newProject.printDONEConsole();
		}
		newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/Makefile file");
		block.setFilesourcename("Makefile");
		block.setDirlist("src src-doc");
		block.setSepecho("=");
		if (!newProjectGenFiles.genTemplate(5, block))
			return;
		newProject.printDONEConsole();
		newProject.printHEADERConsole("Documentation sub-tree.");
		block.init();
		block.setFilelocation(projectroot + "/src-doc");
		newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/src-doc directory");
		if (!makedir(projectroot + "/src-doc")) {
			newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src-doc");
			newProject.printERRORConsole();
			return;
		}
		newProject.printDONEConsole();
		newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src-doc/Doxygen.cfg file");
		block.setFilesourcename("Doxygen.cfg");
		if (!newProjectGenFiles.genTemplate(21, block))
			return;
		newProject.printDONEConsole();
		newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src-doc/Makefile file");
		block.setFilesourcename("Makefile");
		if (!newProjectGenFiles.genTemplate(22, block))
			return;
		newProject.printDONEConsole();
		newProject.printHEADERConsole("Source sub-tree.");
		block.init();
		block.setFilelocation(projectroot + "/src");
		newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/src directory");
		if (!makedir(projectroot + "/src")) {
			newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src");
			newProject.printERRORConsole();
			return;
		}
		newProject.printDONEConsole();
		newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/Makefile file");
		block.setFilesourcename("Makefile");
		block.setDirlist(newProject.StaticLibraries + " " + newProject.SharedLibraries + " " + newProject.GGOExecutables
				+ " " + newProject.Executables);
		block.setSepecho("-");
		if (!newProjectGenFiles.genTemplate(5, block))
			return;
		newProject.printDONEConsole();
		newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/macros.h file");
		block.setFilesourcename("macros.h");
		if (!newProjectGenFiles.genTemplate(14, block))
			return;
		newProject.printDONEConsole();
		if (newProject.FactorizedMakefile) {
			if (newProject.StaticLibraries.length() != 0) {
				newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/src/lib.mk file");
				block.setFilesourcename("lib.mk");
				if (!newProjectGenFiles.genTemplate(16, block))
					return;
				newProject.printDONEConsole();
			}
			if (newProject.SharedLibraries.length() != 0) {
				newProject.printITEMConsole("|    Creating " + newProject.ProjectName + "/src/sharedlib.mk file");
				block.setFilesourcename("sharedlib.mk");
				if (!newProjectGenFiles.genTemplate(26, block))
					return;
				newProject.printDONEConsole();
			}
			if (newProject.GGOExecutables.length() != 0) {
				String libobj = "";
				String alib[] = newProject.StaticLibraries.split(" ");
				for (String ¢ : alib)
					libobj += "\\$(OBJDIR)/" + ¢ + ".a ";
				alib = newProject.SharedLibraries.split(" ");
				for (String ¢ : alib)
					libobj += "\\$(OBJDIR)/" + ¢ + ".so ";
				newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/execggo.mk file");
				block.setFilesourcename("execggo.mk");
				block.setLibraries(libobj);
				if (!newProjectGenFiles.genTemplate(19, block))
					return;
				newProject.printDONEConsole();
			}
			if (newProject.Executables.length() != 0) {
				String libobj = "";
				String alib[] = newProject.StaticLibraries.split(" ");
				for (String ¢ : alib)
					libobj += "\\$(OBJDIR)/" + ¢ + ".a ";
				alib = newProject.SharedLibraries.split(" ");
				for (String ¢ : alib)
					libobj += "\\$(OBJDIR)/" + ¢ + ".so ";
				newProject.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/exec.mk file");
				block.setFilesourcename("exec.mk");
				block.setLibraries(libobj);
				if (!newProjectGenFiles.genTemplate(15, block))
					return;
				newProject.printDONEConsole();
			}
		}
		block.init();
		if (newProject.StaticLibraries.length() != 0)
			for (String d : newProject.StaticLibraries.split(" ")) {
				newProject
						.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/" + d + " directory");
				if (!makedir(projectroot + "/src/" + d)) {
					newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src/" + d);
					newProject.printERRORConsole();
					return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + slash + d + "_phoney.c file");
				block.setFilelocation(projectroot + "/src/" + d);
				block.setFilesourcename(d + "_phoney.c");
				block.setTargetlib(d + "_phoney");
				if (!newProjectGenFiles.genTemplate(24, block))
					return;
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + "/Makefile file");
				block.setFilesourcename("Makefile");
				block.setSourceList(d + "_phoney.c");
				block.setTargetlib(d + ".a");
				if (newProject.FactorizedMakefile) {
					if (!newProjectGenFiles.genTemplate(18, block))
						return;
				} else if (!newProjectGenFiles.genTemplate(7, block))
					return;
				newProject.printDONEConsole();
			}
		block.init();
		if (newProject.SharedLibraries.length() != 0)
			for (String d : newProject.SharedLibraries.split(" ")) {
				newProject
						.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/" + d + " directory");
				if (!makedir(projectroot + "/src/" + d)) {
					newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src/" + d);
					newProject.printERRORConsole();
					return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + slash + d + "_phoney.c file");
				block.setFilelocation(projectroot + "/src/" + d);
				block.setFilesourcename(d + "_phoney.c");
				block.setTargetlib(d + "_phoney");
				if (!newProjectGenFiles.genTemplate(24, block))
					return;
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + "/Makefile file");
				block.setFilesourcename("Makefile");
				block.setSourceList(d + "_phoney.c");
				block.setTargetlib(d + ".so");
				if (newProject.FactorizedMakefile) {
					if (!newProjectGenFiles.genTemplate(27, block))
						return;
				} else if (!newProjectGenFiles.genTemplate(25, block))
					return;
				newProject.printDONEConsole();
			}
		block.init();
		if (newProject.GGOExecutables.length() != 0) {
			String libobj = "";
			String alib[] = newProject.StaticLibraries.split(" ");
			for (String ¢ : alib)
				libobj += "\\$(OBJDIR)/" + ¢ + ".a ";
			alib = newProject.SharedLibraries.split(" ");
			for (String ¢ : alib)
				libobj += "\\$(OBJDIR)/" + ¢ + ".so ";
			for (String d : newProject.GGOExecutables.split(" ")) {
				newProject
						.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/" + d + " directory");
				if (!makedir(projectroot + "/src/" + d)) {
					newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src/" + d);
					newProject.printERRORConsole();
					return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + "/Makefile file");
				block.setFilelocation(projectroot + "/src/" + d);
				block.setFilesourcename("Makefile");
				block.setSourceList(d + ".c");
				block.setBasesourcename(d);
				if (newProject.FactorizedMakefile) {
					if (!newProjectGenFiles.genTemplate(20, block))
						return;
				} else {
					block.setLibraries(libobj);
					if (!newProjectGenFiles.genTemplate(8, block))
						return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + slash + d + ".c file");
				block.setFilesourcename(d + ".c");
				block.setBasesourcename(d);
				if (!newProjectGenFiles.genTemplate(4, block))
					return;
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + slash + d + ".ggo file");
				block.setFilesourcename(d + ".ggo");
				if (!newProjectGenFiles.genTemplate(13, block))
					return;
				newProject.printDONEConsole();
			}
		}
		block.init();
		if (newProject.Executables.length() != 0) {
			String libobj = "";
			String alib[] = newProject.StaticLibraries.split(" ");
			for (String ¢ : alib)
				libobj += "\\$(OBJDIR)/" + ¢ + ".a ";
			alib = newProject.SharedLibraries.split(" ");
			for (String ¢ : alib)
				libobj += "\\$(OBJDIR)/" + ¢ + ".so ";
			for (String d : newProject.Executables.split(" ")) {
				newProject
						.printITEMConsole("|    |    Creating " + newProject.ProjectName + "/src/" + d + " directory");
				if (!makedir(projectroot + "/src/" + d)) {
					newProject.ErrorBox("Cannot create directory ", newProject.ProjectName + "/src/" + d);
					newProject.printERRORConsole();
					return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + "/Makefile file");
				block.setFilelocation(projectroot + "/src/" + d);
				block.setFilesourcename("Makefile");
				block.setBasesourcename(d);
				block.setSourceList(d + ".c");
				if (newProject.FactorizedMakefile) {
					if (!newProjectGenFiles.genTemplate(17, block))
						return;
				} else {
					block.setLibraries(libobj);
					if (!newProjectGenFiles.genTemplate(6, block))
						return;
				}
				newProject.printDONEConsole();
				newProject.printITEMConsole(
						"|    |    |    Creating " + newProject.ProjectName + "/src/" + d + slash + d + ".c file");
				block.setFilesourcename(d + ".c");
				if (!newProjectGenFiles.genTemplate(1, block))
					return;
				newProject.printDONEConsole();
			}
		}
		newProject.printHEADERConsole("END.");
	}
	//--------------------------------------------------------------------------
	private static Boolean makedir(String dirname) {
		File d;
		Boolean b = false;
		try {
			d= new File(dirname);
			b = d.mkdirs();
		}
		catch(Exception e){
			newProject.ErrorBox("Cannot create directory '" + dirname + "'", e.getMessage());
		}
		return(b);
	}
	//--------------------------------------------------------------------------
	private static String executeCommand(String command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = reader.readLine())!= null) {
				if (line.length() != 0) {
					output.append("|        " + line);
				}
			}
		} catch (Exception e) {
			newProject.ErrorBox("Cannot exec command '" + command + "'", e.getMessage());
		}
		return output.toString();
	}
}
//==============================================================================
