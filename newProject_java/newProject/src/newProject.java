//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
// 
// This program is free software; you can redistribute it and/or modify it under the terms
// of the GNU General Public License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

class newProject {
	//--- Private data ---------------------------------------------------------
	private static final String eol = System.getProperty("line.separator");
	private final static String slash = System.getProperty("file.separator");
	private static final String CurrentDirectory = System.getProperty("user.dir");

	private static final String sName = "newProject";
	private static final String sTitle = "C PROJECT AND SOURCE FILE CREATION FROM TEMPLATES";
	private static final String sTitleShell = "C Project And Source File Creation From Templates";
	private static final String sPropertySuffix = ".properties";
	private static final String sProperty = sName + sPropertySuffix;
	private static final String dirBin = "newProject_bin";
	private static final String dirData = "newProject_data";
	//--------------------------------------------------------------------------
	private static final String propertyAuthor = "AUTHOR";
	private static final String propertyYear = "YEAR";
	private static final String propertyGit = "GIT";
	private static final String propertyCopyright = "COPYRIGHT";

	private static final String property_Author = "John Doe";
	private static final String property_Year = "1953-2016";
	private static final String property_Git = "";
	private static final String property_Copyright = "Copyright (c) _YEAR_, _AUTHOR_\nAll rights reserved";
	//--------------------------------------------------------------------------
	private static final String intelliJobjpath = "newProject_java/newProject/out/production/newProject/";
	private static String InstallBinDir;
	static String InstallDataDir;
	private static String propertiesfile;

	private static Font fontProperty;
	private static Font fontGroup;
	private static Font fontLabel;
	private static Font fontButton;
	private static Font fontTitle;
	private static Font fontVersion;
	private static Font fontTab;
	private static Font fontConsole;
	//--- Common --------------------------------------------------------------
	static String ProjectName;
	static String ProjectDescription;
	//--- Project --------------------------------------------------------------
	static String ProjectLocation;
	static String StaticLibraries;
	static String SharedLibraries;
	static String GGOExecutables;
	static String Executables;
	static Boolean FactorizedMakefile = false;
	static Boolean README = false;
	static Boolean ChangeLog = false;
	static Boolean TODO = false;
	static Boolean RELEASENOTES = false;
	static Boolean gitignore = false;

	private static Text txtProjectName;
	private static Text txtProjectDescription;
	private static Text txtProjectLocation;
	private static Text txtStaticLibraries;
	private static Text txtSharedLibraries;
	private static Text txtGGOExecutables;
	private static Text txtExecutables;
	private static Boolean txtFactorizedMakefile = false;
	private static Boolean txtREADME = true;
	private static Boolean txtChangeLog = true;
	private static Boolean txtTODO = true;
	private static Boolean txtRELEASENOTES = true;
	private static Boolean txtgitignore = true;
	//--- File -----------------------------------------------------------------
	static String FileLocation;
	static String FileName;
	private static String intermediateFileName;
	private static int FileType = -1;

	private static Text txtFileLocation;
	private static Text txtFileName;
	//--- Property -------------------------------------------------------------
	static String Author;
	static String Year;
	static String Git;
	static String Copyright;

	private static Text txtAuthor;
	private static Text txtYear;
	private static Text txtGit;
	private static Text txtCopyright;
	//--------------------------------------------------------------------------
	private static Display display;
	private static Shell shell;
	private static Button buttonFile;
	private static Label lbtProperty;
	private static Boolean PredefinedFileAlreadySelected = false;
	private static Boolean UserdefinedFileAlreadySelected = false;
	private static Boolean ConsoleAlreadyCreated = false;
	private static Image oldBackImage;
	private static final String ErrorRequired = ": REQUIRED field." + eol + eol + "Try again...";
	private static StyledText console;
	//--- Templates ------------------------------------------------------------
	static final String[] Templates = {
			"01-cmain_template",             //  1 - C main
			"02-clib_template",              //  2 - C routine
			"03-cheader_template",           //  3 - C header
			"04-cmainggo_template",          //  4 - C main GGO
			"05-make-cd_template",           //  5 - Makefile cd (top level and 'src' level)
			"06-make-exec_template",         //  6 - Makefile exec (leaf-directory)
			"07-make-lib_template",          //  7 - Makefile static lib (leaf-directory)
			"08-make-execggo_template",      //  8 - Makefile exec GGO (leaf-directory)
			"09-readme_template",            //  9 - README file (top level)
			"10-todo_template",              // 10 - TODO file (top level)
			"11-changelog_template",         // 11 - ChangeLog file (top level)
			"12-releasenotes_template",      // 12 - Release Notes file (top level)
			"13-ggo_template",               // 13 - GGO file (leaf-directory)
			"14-macros-header_template",     // 14 - macros.h file
			"15-exec-mk_template",           // 15 - exec.mk ('src' level)
			"16-lib-mk_template",            // 16 - lib.mk ('src' level)
			"17-makef-exec_template",        // 17 - Makefile exec (factorized / leaf-directory)
			"18-makef-lib_template",         // 18 - Makefile static lib (factorized / leaf-directory)
			"19-execggo-mk_template",        // 19 - execggo.mk (factorized / 'src' level')
			"20-makef-execggo_template",     // 20 - Makefile-exec GGO (factorized / leaf-directory)
			"21-doxygen-cfg_template",       // 21 - Doxygen configuration file
			"22-make-doc_template",          // 22 - Makefile-doc
			"23-gitignore_template",         // 23 - .gitignore file (top level)
			"24-clibphoney_template",        // 24 - phoney.c file
			"25-make-sharedlib_template",    // 25 - Makefile shared library (leaf-directory)
			"26-sharedlib-mk_template",      // 26 - sharedlib.mk ('src' level)
			"27-makef-sharedlib_template",   // 27 - Makefile shared library (factorized / leaf-directory)
	};
	//==========================================================================
	public static void main(String[] args) {
		// Display
		display = new Display();
		fontProperty = new Font(Display.getDefault(), "Arial", 9, SWT.BOLD);
		fontGroup = new Font(Display.getDefault(), "Arial", 9, SWT.BOLD | SWT.ITALIC);
		fontLabel = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
		fontButton = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
		fontTitle = new Font(Display.getDefault(), "Arial", 20, SWT.BOLD);
		fontVersion = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
		fontTab = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
		fontConsole = new Font(Display.getDefault(), "Ubuntu Mono", 10, SWT.NORMAL);

		// PATH TO THIS FILE and path dependent initializations
		String InstallRootDir = newProject.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		InstallRootDir = InstallRootDir.replace("newProject.jar", "");
		InstallBinDir = InstallRootDir;
		InstallDataDir = InstallRootDir.replace(dirBin, dirData);
		if (InstallRootDir.length() > intelliJobjpath.length()) {
			String s = InstallRootDir.substring(InstallRootDir.length() - intelliJobjpath.length());
			if (s.equals(intelliJobjpath)) {
				InstallRootDir = InstallRootDir.substring(0, InstallRootDir.length() - intelliJobjpath.length() - 1);
				InstallBinDir = InstallRootDir + slash + dirBin + slash;
				InstallDataDir = InstallRootDir + slash + dirData + slash;
			}
		}
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",       false, "print this message and exit");
		options.addOption("V", "version",    false, "print the version information and exit");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitleShell, options);
				Exit(0);
			}
			// Option: Version
			if (line.hasOption("V")) {
				System.out.println(sName + ".jar - Build: " + newProjectBuildInfo.getNumber()
						+ " - Date: " + newProjectBuildInfo.getDate());
				Exit(0);
			}
		} catch (ParseException err) {
			Error("Parsing failed: " + err.getMessage());
		}
		// PROPERTIES
		getProperties();
		if (Git.length() == 0)
			Error("GIT Location: REQUIRED field.");
		// GO ON...
		initUI();
		// EXIT
		Exit(0);
	}
	//==========================================================================
	private static void getProperties() {
		Properties properties = new Properties();
		propertiesfile = InstallBinDir + sProperty;
		File f = new File(propertiesfile);
		// Define and save the properties
		if(! f.isFile()) {
			properties.setProperty(propertyAuthor, property_Author);
			properties.setProperty(propertyYear, property_Year);
			properties.setProperty(propertyGit, property_Git);
			properties.setProperty(propertyCopyright, property_Copyright);
			savePropertyFile(properties, propertiesfile);
		}
		// Load the properties file
		FileInputStream in;
		try {
			in = new FileInputStream(propertiesfile);
			properties.load(in);
			in.close();
		} catch (IOException err) {
			Error("Properties: " + err.getMessage());
		}
		// Convert properties.....
		Author =  properties.getProperty(propertyAuthor, property_Author);
		Year =  properties.getProperty(propertyYear, property_Year);
		Git =  properties.getProperty(propertyGit, property_Git);
		Copyright = properties.getProperty(propertyCopyright, property_Copyright);
	}

	private static void savePropertyFile(Properties p, String filename) {
		FileOutputStream out;
		try {
			out = new FileOutputStream(filename);
			p.store(out, " ---------------------------------------------" + eol
					+ "# " + sName + " properties. Edit and customize them." + eol
					+ "# AUTHOR: String, name(s) of author(s)." + eol
					+ "# YEAR: String, year (or set of year of software writing and/or modification." + eol
					+ "# GIT: String, path of GIT repository." + eol
					+ "# COPYRIGHT: String, copyright clause" + eol
					+ "# ---------------------------------------------");
			out.close();
		} catch (IOException err) { Error(err.getMessage()); }
	}

	private static void Error(String ¢) {
		//System.err.println("!!! ERROR !!! " + ¢);
		ErrorBox(¢, "");
		Exit(-1);
	}

	private static void Exit(int value) {
		if (oldBackImage != null) oldBackImage.dispose();
		if (fontProperty != null) fontProperty.dispose();
		if (fontGroup != null) fontGroup.dispose();
		if (fontLabel != null) fontLabel.dispose();
		if (fontTitle != null) fontTitle.dispose();
		if (fontVersion != null) fontVersion.dispose();
		if (fontButton != null) fontButton.dispose();
		if (fontTab != null) fontTab.dispose();
		if (fontConsole != null) fontConsole.dispose();
		if (display != null) display.dispose();
		System.exit(value);
	}

	static void ErrorBox(String Message1, String Message2) {
		if (shell == null)
			shell = new Shell(display, SWT.NONE);
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(Message1 + ": " + Message2);
		messageBox.open();
	}
	//==========================================================================
	private static void initUI() {
		shell = new Shell(display, SWT.MAX | SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.RESIZE);
		shell.setText(sTitleShell);
		shell.setLayout(new GridLayout());
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.addListener(SWT.Resize, __ -> {
            Rectangle rect = shell.getClientArea();
            Image newImage = new Image(display, Math.max(1, rect.width), 1);
            GC gc = new GC(newImage);
            gc.setForeground(display.getSystemColor(SWT.COLOR_GRAY));
            gc.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
            gc.fillGradientRectangle(rect.x, rect.y, rect.width, 1, false);
            gc.dispose();
            shell.setBackgroundImage(newImage);
            if (oldBackImage != null) oldBackImage.dispose();
            oldBackImage = newImage;
        });
		// Graphical area
		Composite container = new Composite(shell, SWT.FILL);
		container.setLayout(new GridLayout());
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));

		initTitle(container);
		initVersion(container);
		initProject(container);
		CTabFolder tabFolders = initTabFolder(container); 
		CTabItem folderProj = initFolderProj(tabFolders);
			initProjectStructure(tabFolders, folderProj);
		CTabItem folderFile = initFolderFile(tabFolders);
			initFileList(tabFolders, folderFile);
		CTabItem folderProperty = initFolderProperty(tabFolders);
			initProperty(tabFolders, folderProperty);
		initButtonBar(tabFolders, container);
		tabFolders.setSelection(0);
		tabFolders.pack();

		// Go on
		shell.pack();
		shell.setSize(900, 750);
		shell.setMinimumSize(900, 750); 
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}

	private static void initTitle(Composite parent) {
		Label title = new Label(parent, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		title.setText(sTitle);
		title.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		title.setFont(fontTitle);
	}

	private static void initVersion(Composite parent) {
		Composite compositeA2 = new Composite(parent, SWT.FILL | SWT.BORDER);
		compositeA2.setLayout(new GridLayout());
		compositeA2.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));

		Label labA21 = new Label(compositeA2, SWT.CENTER);
		labA21.setFont(fontVersion);
		labA21.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		labA21.setText("Build " + newProjectBuildInfo.getNumber() + " - " + newProjectBuildInfo.getDate());
	}

	private static void initProject(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		createProperty(container);
		createProjectName(container);
		createProjectDescription(container);
	}

	private static CTabFolder initTabFolder(Composite parent) {
		CTabFolder $ = new CTabFolder(parent, SWT.BORDER);
		$.setBorderVisible(false);
		$.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));
		$.setSimple(false);
		return $;
	}

	private static void initButtonBar(CTabFolder f, Composite parent) {
		Composite buttonBar = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		layout.makeColumnsEqualWidth = true;
		buttonBar.setLayout(layout);

		final GridData data = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = false;
		buttonBar.setLayoutData(data);

		Label labinvisible1 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible2 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible3 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible3.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible4 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible4.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		Button quit = new Button(buttonBar, SWT.PUSH);
		quit.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		quit.setText("Quit");
		quit.setFont(fontButton);
		quit.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		quit.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		quit.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				Exit(0);
			}
		});

		Button ok = new Button(buttonBar, SWT.PUSH);
		ok.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ok.setText("GO");
		ok.setFont(fontButton);
		ok.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		ok.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (f.getSelection() == f.getItem(0)) { // Project
					saveInputProject();
					if (ProjectName.length() == 0 ) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("Project Name" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (ProjectDescription.length() == 0 ) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("Project Description" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (ProjectLocation.length() == 0 ) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("Project Location:" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (! ConsoleAlreadyCreated) {
						createConsole();
						ConsoleAlreadyCreated = true;
					}
					newProjectGenProject.genProject();
				} else if (f.getSelection() != f.getItem(1)) {
					saveInputProperty();
					if (Git.length() == 0) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(),
								SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("GIT Location" + ErrorRequired);
						messageBox.open();
						return;
					}
					lbtProperty.setText("AUTHOR=" + Author + " - YEAR=" + Year + " - GIT=" + Git);
					lbtProperty.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
					lbtProperty.getParent().layout();
					Properties properties = new Properties();
					properties.setProperty(propertyAuthor, Author);
					properties.setProperty(propertyYear, Year);
					properties.setProperty(propertyGit, Git);
					properties.setProperty(propertyCopyright, Copyright);
					savePropertyFile(properties, propertiesfile);
					savePropertyFile(properties, propertiesfile);
				} else {
					if (txtFileName.getEnabled())
						intermediateFileName = txtFileName.getText();
					saveInputFile();
					if (ProjectName.length() == 0) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(),
								SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("Project Name" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (ProjectDescription.length() == 0) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(),
								SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("Project Description" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (FileLocation.length() == 0) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(),
								SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("File Location:" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (FileName.length() == 0) {
						MessageBox messageBox = new MessageBox(display.getActiveShell(),
								SWT.OK | SWT.ICON_ERROR);
						messageBox.setMessage("File Name:" + ErrorRequired);
						messageBox.open();
						return;
					}
					if (!ConsoleAlreadyCreated) {
						createConsole();
						ConsoleAlreadyCreated = true;
					}
					newProjectGenFiles.genFile(FileType);
				}
			}
		});
	}

	private static void createProperty(Composite container) {
		Composite area = new Composite(container, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(1, false));

		lbtProperty = new Label(area, SWT.NONE);
		lbtProperty.setText("AUTHOR=" + Author + " - YEAR=" + Year + " - GIT=" + Git);
		lbtProperty.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtProperty.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true));
		lbtProperty.setFont(fontProperty);
	}

	private static void createProjectName(Composite container) {
		Composite area = new Composite(container, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label lbtProjectName = new Label(area, SWT.NONE);
		lbtProjectName.setText("Project name: ");
		lbtProjectName.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtProjectName.setFont(fontLabel);

		GridData dataProjectName = new GridData();
		dataProjectName.grabExcessHorizontalSpace = true;
		dataProjectName.horizontalAlignment = GridData.FILL;

		txtProjectName = new Text(area, SWT.BORDER);
		txtProjectName.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtProjectName.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtProjectName.setLayoutData(dataProjectName);

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Name of project to create. It must conform " +
						"to the rules for naming  directories. A directory of that name " +
						"will be created in the current directory and will be the root of " +
						"the project tree. The project name will appear in the header of each source file.");
				messageBox.open();
			}
		});
	}

	private static void createProjectDescription(Composite container) {
		Composite area = new Composite(container, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label lbtProjectDescription = new Label(area, SWT.NONE);
		lbtProjectDescription.setText("Project one-line description: ");
		lbtProjectDescription.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtProjectDescription.setFont(fontLabel);

		GridData dataProjectDescription = new GridData();
		dataProjectDescription.grabExcessHorizontalSpace = true;
		dataProjectDescription.horizontalAlignment = GridData.FILL;

		txtProjectDescription = new Text(area, SWT.BORDER);
		txtProjectDescription.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtProjectDescription.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtProjectDescription.setLayoutData(dataProjectDescription);

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. One-line description of the project. " +
						"The description will appear in the header of each source file.");
				messageBox.open();
			}
		});
	}

	private static void createMargin(Composite container) {
		(new Label(container, SWT.NO_FOCUS | SWT.HIDE_SELECTION))
				.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
	}

	private static void createConsole() {
		Shell shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setText(sTitleShell);
		shell.setLayout(new GridLayout());
		shell.setSize(820, 490);

		shell.addListener(SWT.Close, __ -> ConsoleAlreadyCreated = false);

		console = new StyledText(shell, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.minimumHeight = gridData.heightHint = 20 * console.getLineHeight();
		gridData.minimumWidth = gridData.widthHint = 650;	// ~ 80 chars
		console.setLayoutData(gridData);
		console.setBackground(display.getSystemColor(SWT.COLOR_BLACK));
		console.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		console.setFont(fontConsole);
		console.setEditable(false);
		console.setEnabled(true);

		shell.pack();
		shell.open();
	}
	static void printITEMConsole(String label) {
		console.append(String.format("%-80s", label));
	}
	static void printlnConsole(String label) {
		console.append(label + eol);
		console.setSelection(console.getText().length());
	}
	static void printHEADERConsole(String label) {
		StyleRange styleRange = new StyleRange();
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		styleRange.fontStyle = SWT.BOLD;
		console.append(label + eol);
		console.setStyleRange(styleRange);
		console.setSelection(console.getText().length());
	}
	static void printDONEConsole() {
		String label = "DONE";

		StyleRange styleRange = new StyleRange();
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		styleRange.fontStyle = SWT.BOLD;
		styleRange.foreground = display.getSystemColor(SWT.COLOR_GREEN);
		console.append(label + eol);
		console.setStyleRange(styleRange);
		console.setSelection(console.getText().length());
	}
	static void printERRORConsole() {
		String label = "ERROR";
	
		StyleRange styleRange = new StyleRange();
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		styleRange.fontStyle = SWT.BOLD;
		styleRange.foreground = display.getSystemColor(SWT.COLOR_RED);
		console.append(label + eol);
		console.setStyleRange(styleRange);
		console.setSelection(console.getText().length());
	}
	//=== Project Tab ==========================================================
	private static CTabItem initFolderProj(CTabFolder parent) {
		CTabItem $ = new CTabItem (parent, SWT.NULL);
		$.setText(" Project ");
		$.setFont(fontTab);
		return $;
	}

	private static void initProjectStructure(CTabFolder parent, CTabItem folder) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		folder.setControl(container);

		createProjectLocation(container);
		createTargetStaticLibrary(container);
		createTargetSharedLibrary(container);
		createTargetExecutable(container);
		createMakefile(container);
		createAdditionalFiles(container);
		createMargin(container);
	}

	private static void createProjectLocation(Composite container) {
		Composite area = new Composite(container, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(4, false));

		Label lbtProjectLocation = new Label(area, SWT.NONE);
		lbtProjectLocation.setText("Project location: ");
		lbtProjectLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtProjectLocation.setFont(fontLabel);

		GridData dataProjectLocation = new GridData();
		dataProjectLocation.grabExcessHorizontalSpace = true;
		dataProjectLocation.horizontalAlignment = GridData.FILL;

		txtProjectLocation = new Text(area, SWT.BORDER);
		txtProjectLocation.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtProjectLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtProjectLocation.setLayoutData(dataProjectLocation);

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		open.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(display.getActiveShell(), SWT.OPEN);
				dlg.setFilterPath(CurrentDirectory);
				String fn = dlg.open();
				if (fn != null)
					txtProjectLocation.setText(fn);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Path of the directory where the project will be created.");
				messageBox.open();
			}
		});
	}

	private static void createTargetStaticLibrary(Composite container) {
		Group parameters = new Group(container, SWT.NONE);
		parameters.setFont(fontGroup);
		parameters.setText("  TARGET=static library  ");
		parameters.setLayout(new GridLayout(3, false));
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Label lbtStatic = new Label(parameters, SWT.NONE);
		lbtStatic.setText("Space-separated sub-directories names: ");
		lbtStatic.setFont(fontLabel);
		lbtStatic.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		GridData dataStatic = new GridData();
		dataStatic.grabExcessHorizontalSpace = true;
		dataStatic.horizontalAlignment = GridData.FILL;

		txtStaticLibraries = new Text(parameters, SWT.BORDER);
		txtStaticLibraries.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtStaticLibraries.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtStaticLibraries.setLayoutData(dataStatic);

		Button help = new Button(parameters, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Optional field. List of directories whose goal is to deliver a static " +
						"library. The names must be separated by the character ' '. Each library " +
						"will be named <directory name>.a");
				messageBox.open();
			}
		});
	}

	private static void createTargetSharedLibrary(Composite container) {
		Group parameters = new Group(container, SWT.NONE);
		parameters.setFont(fontGroup);
		parameters.setText("  TARGET=shared library  ");
		parameters.setLayout(new GridLayout(3, false));
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Label lbtShared = new Label(parameters, SWT.NONE);
		lbtShared.setText("Space-separated sub-directories names: ");
		lbtShared.setFont(fontLabel);
		lbtShared.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		GridData dataShared = new GridData();
		dataShared.grabExcessHorizontalSpace = true;
		dataShared.horizontalAlignment = GridData.FILL;

		txtSharedLibraries = new Text(parameters, SWT.BORDER);
		txtSharedLibraries.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtSharedLibraries.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtSharedLibraries.setLayoutData(dataShared);

		Button help = new Button(parameters, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Optional field. List of directories whose goal is to deliver a shared " +
						"library. The names must be separated by the character ','. Each library " +
						"will be named <directory name>.so");
				messageBox.open();
			}
		});
	}

	private static void createTargetExecutable(Composite container) {
		Group parameters = new Group(container, SWT.NONE);
		parameters.setFont(fontGroup);
		parameters.setText("  TARGET=executable  ");
		parameters.setLayout(new GridLayout(3, false));
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Label lbtGgoExec = new Label(parameters, SWT.NONE);
		lbtGgoExec.setText("Space-separated GGO sub-directories names: ");
		lbtGgoExec.setFont(fontLabel);
		lbtGgoExec.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		GridData dataExec = new GridData();
		dataExec.grabExcessHorizontalSpace = true;
		dataExec.horizontalAlignment = GridData.FILL;

		txtGGOExecutables = new Text(parameters, SWT.BORDER);
		txtGGOExecutables.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtGGOExecutables.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtGGOExecutables.setLayoutData(dataExec);

		Button help = new Button(parameters, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Optional field. List of directories whose goal is to deliver an executable " +
						"based on 'Gengetopt' (GGO), a GNU tool to write command line option parsing " +
						"code for C programs. Each executable will have the directory name.");
				messageBox.open();
			}
		});

		Label lbtNotGgoExec = new Label(parameters, SWT.NONE);
		lbtNotGgoExec.setText("Space-separated non-GGO sub-directories names: ");
		lbtNotGgoExec.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtNotGgoExec.setFont(fontLabel);

		txtExecutables = new Text(parameters, SWT.BORDER);
		txtExecutables.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtExecutables.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtExecutables.setLayoutData(dataExec);

		Button help2 = new Button(parameters, SWT.PUSH);
		help2.setText("Help");
		help2.setFont(fontLabel);
		help2.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help2.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Optional field. List of directories whose goal is to deliver " +
						"an executable using 'getopt_long' to parse command line options. Each " +
						"executable will have the directory name.");
				messageBox.open();
			}
		});
	}

	private static void createMakefile(Composite container) {
		Group parameters = new Group(container, SWT.NONE);
		parameters.setFont(fontGroup);
		parameters.setText("  Makefile  ");
		parameters.setLayout(new GridLayout(4, false));
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Label lbtMakefile = new Label(parameters, SWT.NONE);
		lbtMakefile.setText("Factorized makefiles: ");
		lbtMakefile.setFont(fontLabel);

		Button checkFactor = new Button(parameters, SWT.CHECK);
		checkFactor.setText("Yes");
		checkFactor.setFont(fontLabel);
		checkFactor.setSelection(txtFactorizedMakefile);
		checkFactor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtFactorizedMakefile = btn.getSelection();
			}
		});

		GridData dataExec = new GridData();
		dataExec.grabExcessHorizontalSpace = true;
		dataExec.horizontalAlignment = GridData.FILL;
		Label lbtInvisible = new Label(parameters, SWT.NONE);
		lbtInvisible.setLayoutData(dataExec);
		
		Button help = new Button(parameters, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("You have at your disposal 2 systems to generate the 'Makefiles' " +
					"in each leaf-directory." + eol + eol +
					"The first ('no factorization'), generates 1 makefile " +
					"by leaf-directory. In the case where the project delivers several deliveries " +
					"with the same type (several libraries or executables), these makefiles are " +
					"more or less the same. In case of modification, you need to act on all of them." + eol + eol +
					"The second ('factorization') allows to factorize at the 'src' directory level, all " +
					"definitions and rules common to different makefiles: 'exec.mk' and 'lib.mk' files. " +
					"The makefiles generated in each leaf directory are then very simple because they only " +
					"define their dedicated data and reference these common parts (via an 'include' primitive).");
				messageBox.open();
			}
		});
	}

	private static void createAdditionalFiles(Composite container) {
		Group parameters = new Group(container, SWT.NONE);
		parameters.setFont(fontGroup);
		parameters.setText("  Additional top-level files  ");
		parameters.setLayout(new GridLayout(5, false));
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));
		
		Button checkReadme = new Button(parameters, SWT.CHECK);
		checkReadme.setText("README");
		checkReadme.setFont(fontLabel);
		checkReadme.setSelection(txtREADME);
		checkReadme.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtREADME = btn.getSelection();
			}
		});
		Button checkChangelog = new Button(parameters, SWT.CHECK);
		checkChangelog.setText("ChangeLog");
		checkChangelog.setFont(fontLabel);
		checkChangelog.setSelection(txtChangeLog);
		checkChangelog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtChangeLog = btn.getSelection();
			}
		});
		Button checkTodo = new Button(parameters, SWT.CHECK);
		checkTodo.setText("TODO");
		checkTodo.setFont(fontLabel);
		checkTodo.setSelection(txtTODO);
		checkTodo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtTODO = btn.getSelection();
			}
		});
		Button checkrlnotes = new Button(parameters, SWT.CHECK);
		checkrlnotes.setText("RELEASE NOTES");
		checkrlnotes.setFont(fontLabel);
		checkrlnotes.setSelection(txtRELEASENOTES);
		checkrlnotes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtRELEASENOTES = btn.getSelection();
			}
		});
		Button checkGitignore = new Button(parameters, SWT.CHECK);
		checkGitignore.setText(".gitignore");
		checkGitignore.setFont(fontLabel);
		checkGitignore.setSelection(txtgitignore);
		checkGitignore.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				txtgitignore = btn.getSelection();
			}
		});
	}

	private static void saveInputProject() {
		ProjectName = txtProjectName.getText();
		ProjectDescription = txtProjectDescription.getText().replaceAll("\"", "'").replaceAll(" +", " ");
		ProjectLocation = txtProjectLocation.getText();
		StaticLibraries = txtStaticLibraries.getText().replaceAll(" +", " ");
		SharedLibraries = txtSharedLibraries.getText().replaceAll(" +", " ");
		GGOExecutables = txtGGOExecutables.getText().replaceAll(" +", " ");
		Executables = txtExecutables.getText().replaceAll(" +", " ");
		FactorizedMakefile = txtFactorizedMakefile;
		README = txtREADME;
		ChangeLog = txtChangeLog;
		TODO = txtTODO;
		RELEASENOTES = txtRELEASENOTES;
		gitignore = txtgitignore;
	}
	//=== File Tab =============================================================
	private static CTabItem initFolderFile(CTabFolder parent) {
		CTabItem $ = new CTabItem (parent, SWT.NULL);
		$.setText("  File  ");
		$.setFont(fontTab);
		return $;
	}

	private static void initFileList(CTabFolder parent, CTabItem folder) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		folder.setControl(container);

		createFileLocation(container);
		createDefinedFiles(container);
		createUserDefinedFiles(container);
	}

	private static void createFileLocation(Composite container) {
		Composite area = new Composite(container, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(4, false));

		Label lbtFileLocation = new Label(area, SWT.NONE);
		lbtFileLocation.setText("File location: ");
		lbtFileLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtFileLocation.setFont(fontLabel);

		GridData dataProjectLocation = new GridData();
		dataProjectLocation.grabExcessHorizontalSpace = true;
		dataProjectLocation.horizontalAlignment = GridData.FILL;

		txtFileLocation = new Text(area, SWT.BORDER);
		txtFileLocation.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtFileLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtFileLocation.setLayoutData(dataProjectLocation);

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		open.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(display.getActiveShell(), SWT.OPEN);
				dlg.setFilterPath(CurrentDirectory);
				String fn = dlg.open();
				if (fn != null)
					txtFileLocation.setText(fn);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Path of the directory where the file will be created.");
				messageBox.open();
			}
		});
	}

	private static void createDefinedFiles(Composite container) {
		List<newProjectDefinedFiles> predefinedFiles = new ArrayList<>();
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (exec.)", "Makefile", 6));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (GGO exec.)", "Makefile", 8));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (exec. & exec.mk)", "Makefile", 17));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (GGO exec. & exec.mk)", "Makefile", 20));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (static lib.)", "Makefile", 7));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (shared lib.)", "Makefile", 25));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (static lib. & lib.mk)", "Makefile", 18));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (shared lib. & sharedlib.mk)", "Makefile", 27));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - leaf dir. (Doxygen doc)", "Makefile", 22));
		predefinedFiles.add(new newProjectDefinedFiles("Makefile - intermediate dir.", "Makefile", 5));
		predefinedFiles.add(new newProjectDefinedFiles("Part of Makefile - 'exec.mk' file", "exec.mk", 15));
		predefinedFiles.add(new newProjectDefinedFiles("Part of Makefile - 'execggo.mk' file", "execggo.mk", 19));
		predefinedFiles.add(new newProjectDefinedFiles("Part of Makefile - 'lib.mk' file", "exec.mk", 16));
		predefinedFiles.add(new newProjectDefinedFiles("Part of Makefile - 'sharedlib.mk' file", "sharedlib.mk", 26));
		predefinedFiles.add(new newProjectDefinedFiles("README file", "README.md", 9));
		predefinedFiles.add(new newProjectDefinedFiles("TODO file", "TODO", 10));
		predefinedFiles.add(new newProjectDefinedFiles("ChangeLog file", "CHANGELOG", 11));
		predefinedFiles.add(new newProjectDefinedFiles("Release Notes file", "RELEASENOTES", 12));
		predefinedFiles.add(new newProjectDefinedFiles("'macros.h' header file", "macros.h", 14));
		predefinedFiles.add(new newProjectDefinedFiles("Doxygen configuration file", "Doxygen.cfg", 21));
		predefinedFiles.add(new newProjectDefinedFiles(".gitignore file", ".gitignore", 23));

		Group predefined = new Group(container, SWT.NONE);
		predefined.setFont(fontGroup);
		predefined.setText("  Predefined files  ");
		predefined.setLayout(new GridLayout(3, false));
		predefined.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		SelectionListener selectionListener = new SelectionAdapter () {
			public void widgetSelected(SelectionEvent e) {
				Button button = ((Button) e.widget);
				if (!button.getSelection())
					return;
				if (UserdefinedFileAlreadySelected)
					buttonFile.setSelection(false);
				int i = Integer.valueOf((String) button.getData("Id"));
				intermediateFileName = (predefinedFiles.get(i)).getName();
				FileType = (predefinedFiles.get(i)).getIndex();
				PredefinedFileAlreadySelected = true;
				buttonFile = button;
				txtFileName.setEnabled(false);
			}
		};

		int idx = 0;
		for (newProjectDefinedFiles s : predefinedFiles) {
			Button but = new Button(predefined, SWT.RADIO);
			but.setText(s.getLabel());
			but.setFont(fontLabel);
			but.addSelectionListener(selectionListener);
			but.setData("Id", String.valueOf(idx++));
		}
	}

	private static void createUserDefinedFiles(Composite container) {
		List<newProjectDefinedFiles> predefinedFiles = new ArrayList<>();
		predefinedFiles.add(new newProjectDefinedFiles("C main", "main", 1));
		predefinedFiles.add(new newProjectDefinedFiles("C GGO main", "main", 4));
		predefinedFiles.add(new newProjectDefinedFiles("C function", "function", 2));
		predefinedFiles.add(new newProjectDefinedFiles("C header", "header", 3));
		predefinedFiles.add(new newProjectDefinedFiles("GGO description", "ggo", 13));

		Group userdefined = new Group(container, SWT.NONE);
		userdefined.setFont(fontGroup);
		userdefined.setText("  User-defined files  ");
		userdefined.setLayout(new GridLayout(1, false));
		userdefined.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Composite area1 = new Composite(userdefined, SWT.FILL);
		area1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area1.setLayout(new GridLayout(5, false));

		SelectionListener selectionListener = new SelectionAdapter () {
			public void widgetSelected(SelectionEvent e) {
				Button button = ((Button) e.widget);
				if (!button.getSelection())
					return;
				if (PredefinedFileAlreadySelected)
					buttonFile.setSelection(false);
				int i = Integer.valueOf((String) button.getData("Id"));
				intermediateFileName = (predefinedFiles.get(i)).getName();
				txtFileName.setText(intermediateFileName);
				FileType = (predefinedFiles.get(i)).getIndex();
				UserdefinedFileAlreadySelected = true;
				buttonFile = button;
				txtFileName.setEnabled(true);
			}
		};

		int idx = 0;
		for (newProjectDefinedFiles s : predefinedFiles) {
			Button but = new Button(area1, SWT.RADIO);
			but.setText(s.getLabel());
			but.setFont(fontLabel);
			but.addSelectionListener(selectionListener);
			but.setData("Id", String.valueOf(idx++));
		}
		
		Composite area2 = new Composite(userdefined, SWT.FILL);
		area2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area2.setLayout(new GridLayout(3, false));

		Label lbtFileName = new Label(area2, SWT.NONE);
		lbtFileName.setText("File name (w/o suffix): ");

		lbtFileName.setFont(fontLabel);
		lbtFileName.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		GridData dataFileName = new GridData();
		dataFileName.grabExcessHorizontalSpace = true;
		dataFileName.horizontalAlignment = GridData.FILL;

		txtFileName = new Text(area2, SWT.BORDER);
		txtFileName.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtFileName.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtFileName.setLayoutData(dataFileName);

		Button help = new Button(area2, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Name of file to create.");
				messageBox.open();
			}
		});
	}

	private static void saveInputFile() {
		ProjectName = txtProjectName.getText();
		ProjectDescription = txtProjectDescription.getText().replaceAll("\"", "'").replaceAll(" +", " ");
		FileLocation = txtFileLocation.getText();
		FileName = intermediateFileName;
	}
	//=== Property Tab =========================================================
	private static CTabItem initFolderProperty(CTabFolder parent) {
		CTabItem $ = new CTabItem (parent, SWT.NULL);
		$.setText(" Property ");
		$.setFont(fontTab);
		return $;
	}

	private static void initProperty(CTabFolder parent, CTabItem folder) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		folder.setControl(container);

		Composite area0 = new Composite(container, SWT.FILL);
		area0.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area0.setLayout(new GridLayout(3, false));
		Label lbtAuthor = new Label(area0, SWT.NONE);
		lbtAuthor.setText("Author: ");
		lbtAuthor.setFont(fontLabel);
		lbtAuthor.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		GridData dataAuthor = new GridData();
		dataAuthor.grabExcessHorizontalSpace = true;
		dataAuthor.horizontalAlignment = GridData.FILL;
		txtAuthor = new Text(area0, SWT.BORDER);
		txtAuthor.setLayoutData(dataAuthor);
		txtAuthor.setText(Author);
		txtAuthor.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtAuthor.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		Button help0 = new Button(area0, SWT.PUSH);
		help0.setText("Help");
		help0.setFont(fontLabel);
		help0.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help0.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help0.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Author (or group of authors) to copyright.");
				messageBox.open();
			}
		});

		Composite area1 = new Composite(container, SWT.FILL);
		area1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area1.setLayout(new GridLayout(3, false));
		Label lbtYear = new Label(area1, SWT.NONE);
		lbtYear.setText("Year: ");
		lbtYear.setFont(fontLabel);
		lbtYear.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		GridData dataYear = new GridData();
		dataYear.grabExcessHorizontalSpace = true;
		dataYear.horizontalAlignment = GridData.FILL;
		txtYear = new Text(area1, SWT.BORDER);
		txtYear.setLayoutData(dataYear);
		txtYear.setText(Year);
		txtYear.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtYear.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		Button help1 = new Button(area1, SWT.PUSH);
		help1.setText("Help");
		help1.setFont(fontLabel);
		help1.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help1.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Year (or group of years) to copyright.");
				messageBox.open();
			}
		});

		Composite area2 = new Composite(container, SWT.FILL);
		area2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area2.setLayout(new GridLayout(4, false));
		Label lbtGit = new Label(area2, SWT.NONE);
		lbtGit.setText("GIT database location: ");
		lbtGit.setFont(fontLabel);
		lbtGit.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		GridData dataGit = new GridData();
		dataGit.grabExcessHorizontalSpace = true;
		dataGit.horizontalAlignment = GridData.FILL;
		txtGit = new Text(area2, SWT.BORDER);
		txtGit.setLayoutData(dataGit);
		txtGit.setText(Git);
		txtGit.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtGit.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		Button open = new Button(area2, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		open.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(display.getActiveShell(), SWT.OPEN);
				String fn = dlg.open();
				if (fn == null)
					return;
				txtGit.setText(fn);
				Git = txtGit.getText();
			}
		});
		Button help2 = new Button(area2, SWT.PUSH);
		help2.setText("Help");
		help2.setFont(fontLabel);
		help2.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help2.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Path of the directory where the GIT database is located.");
				messageBox.open();
			}
		});

		Composite area3 = new Composite(container, SWT.FILL);
		area3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area3.setLayout(new GridLayout(3, false));
		Label lbtCopy = new Label(area3, SWT.NONE);
		lbtCopy.setText("Copyright: ");
		lbtCopy.setFont(fontLabel);
		lbtCopy.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		lbtCopy.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		GridData dataCopy = new GridData();
		dataCopy.grabExcessHorizontalSpace = true;
		dataCopy.horizontalAlignment = GridData.FILL;
		dataCopy.heightHint = 150;
		txtCopyright = new Text(area3, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		txtCopyright.setLayoutData(dataCopy);
		txtCopyright.setText(Copyright);
		txtCopyright.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtCopyright.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		Button help3 = new Button(area3, SWT.PUSH);
		help3.setText("Help");
		help3.setFont(fontLabel);
		help3.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		help3.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help3.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Required field. Copyright Clause.");
				messageBox.open();
			}
		});

		Composite area4 = new Composite(container, SWT.FILL);
		area4.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		area4.setLayout(new GridLayout(2, false));
		Label image = new Label(area4, SWT.LEFT);
		image.setImage(display.getSystemImage(SWT.ICON_WARNING));
		image.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		GridData dataWarn = new GridData();
		dataWarn.horizontalAlignment = SWT.BEGINNING;
		dataWarn.verticalAlignment = GridData.BEGINNING;
		dataWarn.grabExcessVerticalSpace = dataWarn.grabExcessHorizontalSpace = true;
		dataWarn.heightHint = 50;
		Label warning = new Label(area4, SWT.LEFT | SWT.WRAP);
		warning.setFont(fontVersion);
		warning.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		warning.setLayoutData(dataWarn);
		warning.setText("WARNING: by clicking the 'GO' button, the configuration file\n\t" +
				propertiesfile + "\nwill be modified.");
	}

	private static void saveInputProperty() {
		Author = txtAuthor.getText().replaceAll(" +", " ");
		Year = txtYear.getText().replaceAll(" +", " ");
		Git = txtGit.getText();
		Copyright = txtCopyright.getText().replaceAll(" +", " ");
	}
}
//==============================================================================
