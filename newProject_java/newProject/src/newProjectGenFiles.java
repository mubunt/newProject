//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
// 
// This program is free software; you can redistribute it and/or modify it under the terms
// of the GNU General Public License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with this program;
// if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

class newProjectGenFiles {
	private final static String slash = System.getProperty("file.separator");
	//--------------------------------------------------------------------------
	static void genFile(int type) {
		newProjectParameterFile block = new newProjectParameterFile();
		String filename = newProject.FileName;

		switch (type) {
		case 1: case 2: case 4: case 24:
			filename += ".c";
			break;
		case 3:
			filename += ".h";
			break;
		case 13:
			filename += ".ggo";
			break;
		default:
			break;
		}
		newProject.printITEMConsole("Creating file '" + newProject.FileLocation + slash + filename + "'");
		if (new File(newProject.FileLocation + slash + filename).isFile()) {
			newProject.ErrorBox("Cannot create file '" + newProject.FileLocation + slash + filename + "'", " already exists");
			newProject.printERRORConsole();
		} else {
			block.init();
			block.setFilelocation(newProject.FileLocation);
			block.setFilesourcename(filename);
			if (genTemplate(type, block)) newProject.printDONEConsole();
		}
	}
	//--------------------------------------------------------------------------
	static Boolean genTemplate(int type, newProjectParameterFile block) {
		String template = newProject.InstallDataDir + newProject.Templates[type - 1];
		if (! new File(template).isFile()) {
			newProject.ErrorBox("File template '" + template + " does not exist.", "");
			newProject.printERRORConsole();
			return false;
		}
		// Create source file
		PrintWriter writer;
		try {
			writer = new PrintWriter(block.getFilelocation() + slash + block.getFilesourcename(), "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			newProject.ErrorBox("Cannot create file '" + block.getFilelocation() + slash + block.getFilesourcename(), e.getMessage());
			newProject.printlnConsole("ERROR");
			return false;
		}
		// Add content
		switch (type) {
		case 1: case 2: case 3:
		case 4: case 14: case 24:
			copyright("C-like", writer);
			break;
		case 5: case 6: case 7: 
		case 8: case 13: case 15:
		case 16: case 17: case 18:
		case 19: case 20: case 22:
		case 25: case 26: case 27:
			copyright("shell-like", writer);
			break;
		case 9: case 10: case 11:
		case 12: case 21: case 23:
		default:
			break;
		}
		copy(template, block, writer);
		// Close source file
		writer.close();
		return true;
	}
	//--------------------------------------------------------------------------
	private static void copyright(String lang, PrintWriter w) {
		String[] cop = newProject.Copyright.split("\\n", -1);
		switch (lang) {
		case "C-like":
			w.println("/**");
			dash(lang, w);
			for (String ¢: cop)
				w.println(" * " + ¢.replaceAll("_AUTHOR_", newProject.Author).replaceAll("_YEAR_", newProject.Year));
			dash(lang, w);
			w.println(" */");
			break;
		case "shell-like":
			dash(lang, w);
			for (String ¢: cop)
				w.println("# " + ¢.replaceAll("_AUTHOR_", newProject.Author).replaceAll("_YEAR_", newProject.Year));
			dash(lang, w);
			break;
		default:
			break;
		}
		w.println("");
	}
	//--------------------------------------------------------------------------
	private static void dash(String lang, PrintWriter w) {
		switch (lang) {
		case "C-like":
			w.println(" *------------------------------------------------------------------------------");
			break;
		case "shell-like":
			w.println("#-------------------------------------------------------------------------------");
			break;
		default:
			break;
		}
	}
	//--------------------------------------------------------------------------
	private static void copy(String from, newProjectParameterFile block, PrintWriter w) {
		String protectHeader = block.getFilesourcename().replaceAll("\\.h", "_H");
		String everything = null;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(from));
		} catch (FileNotFoundException e) {
			newProject.ErrorBox("Cannot open template '" + from, e.getMessage());
		}
		try {
			StringBuilder sb = new StringBuilder();
			for (String line = (br != null) ? br.readLine() : null; line != null;) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			everything = sb + "";
		} catch (IOException e) {
			newProject.ErrorBox("Error while reading template '" + from, e.getMessage());
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
				newProject.ErrorBox("Cannot close template '" + from, e.getMessage());
			}
		}

		w.println(everything != null ? everything.replaceAll("_AUTHOR_", newProject.Author)
						.replaceAll("_YEAR_", newProject.Year)
						.replaceAll("_PROJECT_", newProject.ProjectName)
						.replaceAll("_DESCRIPTION_", newProject.ProjectDescription)
						.replaceAll("_SOURCEFILE_", block.getFilesourcename())
						.replaceAll("_BASENAME_", block.getBasesourcename())
						.replaceAll("_LIBRARY_", block.getTargetlib())
						.replaceAll("_LIBRARIES_", block.getLibraries())
						.replaceAll("_DIRECTORIES_", block.getDirlist())
						.replaceAll("_SOURCELIST_", block.getSourceList())
						.replaceAll("§", block.getSepecho())
						.replaceAll("_PROTECT_", protectHeader)
						.replaceAll("_SOURCEDIRECTORY_", "src") : null
				);
	}
}
//==============================================================================
